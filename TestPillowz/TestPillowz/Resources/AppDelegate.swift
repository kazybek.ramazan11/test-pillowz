//
//  AppDelegate.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 01/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import UIKit
import CoreData
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(rootViewController: ViewController())
        return true
    }
//
    static var persistentContainer: NSPersistentContainer {
        return (UIApplication.shared.delegate as! AppDelegate).persistentContainer
    }
//    
    static var viewContext: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
//    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "Places")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    func confugureInitialViewController() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        if let _ = UserDefaults.standard.string(forKey: "SessionId") {
            window?.rootViewController = ViewController()
        }
        else {
            window?.rootViewController = SignInViewController()
        }
        
    }
}

