//
//  ProductionCountry.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 03/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct ProductionCountry: Codable {
    var iso_3166_1, name: String?
}
