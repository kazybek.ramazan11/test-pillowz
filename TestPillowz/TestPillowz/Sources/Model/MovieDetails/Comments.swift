//
//  Comments.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 03/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct Comments: Codable {
    var id, page: Int?
    var results: [Comment]?
    var total_pages, total_results: Int?
}
