//
//  MovieId.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 04/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
import RealmSwift
class MovieId: Object {
    @objc dynamic var id: Int = 0
}
