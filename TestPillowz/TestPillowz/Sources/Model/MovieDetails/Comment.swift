//
//  Comment.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 04/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct Comment: Codable {
    var id, author, content: String?
}
