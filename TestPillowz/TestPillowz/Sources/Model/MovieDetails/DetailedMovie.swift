//
//  DetailedMovie.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 03/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct DetailedMovie: Codable {
    var adult: Bool?
    var poster_path, backdrop_path: String?
    var genres: [Genre]?
    var id, imbd_id: Int?
    var original_title, overview: String?
    var popularity: Double?
    var release_date: String?
    var runtime: Int?
    var vote_average: Double?
    var vote_count: Int?
    var production_countries: [ProductionCountry]?
}
