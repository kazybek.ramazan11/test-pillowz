//
//  ApiRequest.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 02/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
public enum RequestType: String {
    case GET, POST, DELETE, PATCH
}
protocol ApiRequest {
    var method: RequestType { get }
    var path: String { get }
    var parameters: [String: String] { get }
}
