//
//  Session.swift
//  TestPillowz
//
//  Created by Aibol Tungatarov on 11/28/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation

struct Session: Codable {
    var success: Bool?
    var session_id: String?
    var status_message: String?
    var status_code: Int?
}
