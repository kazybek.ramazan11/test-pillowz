//
//  User.swift
//  TestPillowz
//
//  Created by Aibol Tungatarov on 11/29/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation

struct User {
    var username: String?
    var password: String?
}
