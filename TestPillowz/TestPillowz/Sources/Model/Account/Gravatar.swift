//
//  Gravatar.swift
//  TestPillowz
//
//  Created by Aibol Tungatarov on 11/28/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation

struct Gravatar: Codable {
    var hash: String?
}
