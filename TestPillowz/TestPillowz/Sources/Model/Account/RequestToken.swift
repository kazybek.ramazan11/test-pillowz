//
//  RequestToken.swift
//  TestPillowz
//
//  Created by Aibol Tungatarov on 11/28/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation

struct RequestToken: Codable {
    var success: Bool
    var expires_at: String?
    var request_token: String?
    var status_code: Int?
    var status_message: String?
}
