//
//  SessionWithLogin.swift
//  TestPillowz
//
//  Created by Aibol Tungatarov on 11/28/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation

struct SessionWithLogin: Codable {
    var status_code: Int?
    var status_message: String?
    var success: Bool?
    var expires_at: String?
    var request_token: String?
}
