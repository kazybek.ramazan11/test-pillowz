//
//  Account.swift
//  TestPillowz
//
//  Created by Ramazan Kazybek on 12/22/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct Account: Codable {
    var id: Int?
    var status_code: Int?
    var status_message: String?
    var avatar: Gravatar
    var iso_639_1: String?
    var iso_3166_1: String?
    var name: String?
    var include_adult: Bool?
    var username: String?
}
