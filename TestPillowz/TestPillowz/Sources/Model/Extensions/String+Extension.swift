//
//  String+Extension.swift
//  TestPillowz
//
//  Created by Ramazan Kazybek on 12/23/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
import UIKit
extension String {

    var utfData: Data {
        return Data(utf8)
    }

    var attributedHtmlString: NSAttributedString? {

        do {
            return try NSAttributedString(data: utfData,
            options: [
                      .documentType: NSAttributedString.DocumentType.html,
                      .characterEncoding: String.Encoding.utf8.rawValue
                     ], documentAttributes: nil)
        } catch {
            print("Error:", error)
            return nil
        }
    }
}

extension UILabel {
   func setAttributedHtmlText(_ html: String) {
    if let attributedText = html.attributedHtmlString?.string {
        self.text = attributedText
      }
   }
}
