//
//  HeaderView.swift
//  TestPillowz
//
//  Created by Ramazan Kazybek on 12/23/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    
    lazy var label: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .white
        label.font = UIFont.medium(size: 25)
        return label
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews() {
        backgroundColor = .clear
        addSubview(label)
        label.snp.makeConstraints { (make) in
            make.top.equalToSuperview().offset(8)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
            make.bottom.equalToSuperview()
        }
    }

}
