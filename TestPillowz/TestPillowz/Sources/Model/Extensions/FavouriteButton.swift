//
//  UIButton+Extension.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 04/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
import UIKit
class FavouriteButton: UIButton {
    let isFavouriteImg = UIImage(named: "liked")
    let isNotFavouriteImg = UIImage(named: "not_liked")
    var isFavourite: Bool? {
        didSet {
            guard let isFavourite = isFavourite else { return }
            if isFavourite {
                self.setImage(isFavouriteImg, for: .normal)
            }else {
                self.setImage(isNotFavouriteImg, for: .normal)
            }
        }
    }
}
