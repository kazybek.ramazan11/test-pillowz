//
//  UIFont+Extension.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 02/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    static func medium(size: CGFloat = 27) -> UIFont? {
        let font = UIFont(name: "Arial-BoldMT", size: size)
        return font
    }
    
    static func light(size: CGFloat = 18) -> UIFont? {
        let font = UIFont(name: "ArialMT", size: size)
        return font
    }
}
