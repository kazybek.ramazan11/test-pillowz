//
//  Movies.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 02/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct Movies: Codable {
    var page: Int?
    var total_results: Int?
    var total_pages: Int?
    var results: [Movie]?
}
