//
//  Movie.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 02/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct Movie: Codable {
    var popularity: Double?
    var vote_count, id: Int?
    var video, adult: Bool?
    var poster_path, backdrop_path: String?
    var original_language, original_title: String?
    var genre_ids: [Int]?
    var title: String?
    var vote_average: Double?
    var overview: String?
    var release_date: String?
}
