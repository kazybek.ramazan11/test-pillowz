//
//  Genre.swift
//  TestPillowz
//
//  Created by Казыбек Рамазан on 02/10/2019.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import Foundation
struct Genre: Codable {
    var id: Int
    var name: String?
}
