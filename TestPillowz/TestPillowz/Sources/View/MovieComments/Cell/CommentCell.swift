//
//  CommentCell.swift
//  TestPillowz
//
//  Created by Ramazan Kazybek on 12/22/19.
//  Copyright © 2019 Kazybek Ramazan. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {
    private let userNameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.medium(size: 18)
        label.textColor = .white
        return label
    }()
    
    private let commentLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.light(size: 16)
        label.textColor = UIColor.commentColor
        label.numberOfLines = 0
        return label
    }()
    
    var comment: Comment? {
        didSet {
            userNameLabel.text = comment?.author
            guard let content = comment?.content else {
                return
            }
            commentLabel.setAttributedHtmlText(content)
            
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.selectionStyle = .none
    }
    
    private func configUI() {
        backgroundColor = .clear
        [userNameLabel, commentLabel].forEach {
            addSubview($0)
        }
        makeConstraints()
    }
    
    private func makeConstraints() {
        userNameLabel.snp.makeConstraints { (m) in
            m.top.equalToSuperview().offset(10)
            m.left.equalToSuperview().offset(20)
//            m.right.equalToSuperview().offset(-20)
        }
        
        commentLabel.snp.makeConstraints { (m) in
            m.left.equalTo(userNameLabel)
            m.top.equalTo(userNameLabel.snp.bottom).offset(10)
            m.trailing.lessThanOrEqualToSuperview().offset(-20)
            m.bottom.equalToSuperview().offset(-15).priority(.high)
        }
    }
}
